# Used 3rd Party Packages

## Optional

- Editor Console Pro - V 3.970 - https://assetstore.unity.com/packages/tools/utilities/editor-console-pro-11889

## Required

- Sci-Fi Styled Modular Pack - V 1.1 - https://assetstore.unity.com/packages/3d/environments/sci-fi/sci-fi-styled-modular-pack-82913
  - Ensure to run _Window->Rendering->Render Pipeline Converter_.
    1. Select _Built-in to URP_ in the drop-down up to.
    2. Then check all checkboxes.
    3. Click _Initialize Converters_.
    4. Ensure all assets are checked on. Click _Convert Assets_.
- AllSkyFree - V 1.1.0 - https://assetstore.unity.com/packages/2d/textures-materials/sky/allsky-free-10-sky-skybox-set-146014
- Sci-fi Guns SFX Pack - V 1.0 - https://assetstore.unity.com/packages/audio/sound-fx/sci-fi-guns-sfx-pack-181144
- Nature Sound FX - V 3.0 - https://assetstore.unity.com/packages/audio/sound-fx/nature-sound-fx-180413
- Music - Sad Hope - V 1.0 - https://assetstore.unity.com/packages/audio/music/music-sad-hope-157746
- Grenade Sound FX - V 1.0 - https://assetstore.unity.com/packages/audio/sound-fx/grenade-sound-fx-147490
- Sci-fi GUI skin - V 1.2 - https://assetstore.unity.com/packages/2d/gui/sci-fi-gui-skin-15606
- Sci-Fi Soldier - V 1.2 - https://assetstore.unity.com/packages/3d/characters/humanoids/sci-fi/sci-fi-soldier-29559
  - Once installed use the correct material shader for URP:
    1. Go to  the _Yurowm/Materials_ folder
    2. Open the _Sci-Fi_Rifle_ material and change the _Shader_ to _Universal Render Pipeline/Lit_
    3. Click on the circle left of _Base Map_ under _Surface Inputs_ and find the _Sci-Fi_Rifle_ texture. Double click to use it.
    4. Open the _Sci-Fi_Soldier_ material and change the _Shader_ to _Universal Render Pipeline/Lit_
    5. Click on the circle left of _Base Map_ under _Surface Inputs_ and find the _Sci-Fi_Soldier_ texture. Double click to use it.
