# Simple Shooter for Unity

Implementation based on the Book "Hands-On Unity 2021 Game Development - Second Edition" By Nicolas Alejandro Borromeo from Packt Publishing Ltd.

## Setup

- The '3rd Party Packages' folder needs to contain additional asset packes from the Unity Asset Store. Please refer to the [UsedPackages Readme](Assets/3rd%20Party%20Packages/UsedPackages.md) for which packages are required to be installed in order for the project ot build and work correctly.
  - Make sure to follow any additional installation instructions in that [UsedPackages Readme](Assets/3rd%20Party%20Packages/UsedPackages.md) as well.
- TextMeshPro:
  - Go to _Window / Text Mesh Pro / Font Asset Creator_
  - Click _Import TMP Essentials_
- The used font _Militech_ can be found in the [Resources Directory](Resources/Fonts). Due to licensing limitations it cannot be delivered outside the zip file containing the relevant licensing information.
  - Extract the ttf files in the _militech.zip_ file to the [UI/Fonts](Assets/UI/Fonts) directory
  - Open the _Font Asset Creator_
  - Drage the first font to _Source Font File_
  - Click _Generate Font Atlas_
  - Click _Save_ and save the new font atlas to the _Assets/TextMesh Pro/Resources/Fonts & Materials_ directory
  - Repeast with the second font file

## Requirements

This repository uses GIT LFS for binary files.

This code was implemented and tested on Unity version 2021.2.8f1 as well as Visual Studio 2019.

## Licensing

The C# code in this project is BSD-3 licensed. For specifics on the Unity related files please check with Unity Technologies.
